package sheridan;
/**
 * Bhaumik Patel
 * @author patel
 * Student # 991536277
 * Midterm
 */
public class Celsius {
	
	public static int fromFahrenheit(int value)
	{
		int cel = (5 * (value - 32)) / 9 ;
		return Math.round(cel);
	}

}

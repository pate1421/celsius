package sheridan;
/**
 * Bhaumik Patel
 * Student # 991536277
 * Midterm Exam 
 */
import static org.junit.Assert.*;
import org.junit.Test;
public class CelsiusTest {

	@Test
	public void testFromFahrenheitRegular() {
		int cel = Celsius.fromFahrenheit(100);
		assertTrue("Invalid calculation", cel == 37);
	}
	@Test
	public void testFromFahrenheitBoundaryIn() {
		// let's use a value of 103 and it will give us result of 39.44 which means 
		// it which just in boundary of giving 39.4 means 39
		int cel = Celsius.fromFahrenheit(103);
		assertTrue("Invalid calculation", cel == 39);
	}
	@Test
	public void testFromFahrenheitBoundryOut() {
		// will use 98 so that we will get 36.6 means right out the boundry of 36.5
		int cel = Celsius.fromFahrenheit(98);
		assertTrue("Invalid calculation", cel == 36);
	}
	@Test
	public void testFromFahrenheitException() {
		// here we consider exceptional as we 
		// will use 105 it will give us 40.5 right in the middle
		// here I could have used double value and it would give me a 
		//NumberFormatException but due to the time restriction I am keeping it simple
		int cel = Celsius.fromFahrenheit(105);
		assertTrue("Invalid calculation", cel == 40);
	}
}
